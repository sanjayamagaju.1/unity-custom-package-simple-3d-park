using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class moveObject : MonoBehaviour
{
    public float speed;
    public GameObject forward_btn, backward_btn, left_btn, right_btn;

    // Update is called once per frame
    public void Update()
    {
        if (EventSystem.current.currentSelectedGameObject == forward_btn)
        {
            moveForward();
        }
        else if (EventSystem.current.currentSelectedGameObject == backward_btn)
        {
            moveBackward();
        }
        else if (EventSystem.current.currentSelectedGameObject == left_btn)
        {
            moveLeft();
        }
        else if (EventSystem.current.currentSelectedGameObject == right_btn)
        {
            moveRight();
        }
        else
        {

        }
    }

// move forward
    public void moveForward()
    {
        
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

// move backward
    public void moveBackward()
    {
        transform.Translate(Vector3.back * speed * Time.deltaTime);
    }

// move left
    public void moveLeft()
    {
        transform.Translate(Vector3.left * speed * Time.deltaTime);
    }

// move right
    public void moveRight()
    {
        transform.Translate(Vector3.right * speed * Time.deltaTime);
    }
}
